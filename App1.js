import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert } from 'react-native';

export default function App(){

  const [panjang, setPanjang] = useState(0);
  const [lebar, setLebar] = useState(0);
  const [hasil, setHasil] = useState(0);

  // hitung () {
  //   let jumlah = this.state.panjang*this.state.lebar;
  //   this.setState({hasil:jumlah});
  // }

  return (
    <View style={styles.container}>
      <Text>Test</Text>
      <TextInput onChangeText={text => setPanjang(text)} style={{borderWidth:1,borderColor:'black'}}></TextInput>
      <TextInput onChangeText={text => setLebar(text)} style={{borderWidth:1,borderColor:'black'}}></TextInput>
      <Button
      onPress={()=>setHasil(panjang*lebar)}
      title="Test"
      />
      <Text>{hasil}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
