import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, ToastAndroid } from 'react-native';
import { Card } from 'react-native-elements';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Axios from 'axios';
import { ScrollView } from 'react-native-gesture-handler';

// ini dirubah

function HomeScreen({ navigation: { navigate } }) {
  const [nilaiA, setNilaiA] = useState(0);
  const [nilaiB, setNilaiB] = useState(0);
  const [resultAB, setResultAB] = useState(0);

  function hitung() {
    if (nilaiA == '' && nilaiB == '') {
      ToastAndroid.show('Kotak A & B Harus di isi.', ToastAndroid.SHORT)
    } else
      if (nilaiA == '') {
        ToastAndroid.show('Kotak A Harus di isi.', ToastAndroid.SHORT)
      } else if (nilaiB == '') {
        ToastAndroid.show('Kotak B Harus di isi.', ToastAndroid.SHORT)
      }
      else {
        setResultAB(parseInt(nilaiA) + parseInt(nilaiB))
      }
  }

  return (
    <View style={styles.container}>
      <Text>Tugas Pertama : Penjumlahan</Text>
      <Text></Text>
      <TextInput
        onChangeText={Text => setNilaiA(Text)}
        style={{ textAlign: 'center', height: 40, width: 50, borderWidth: 1, borderColor: 'black' }}
        keyboardType='numeric'
      />
      <Text>+</Text>
      <TextInput
        onChangeText={Text => setNilaiB(Text)}
        style={{ textAlign: 'center', height: 40, width: 50, borderWidth: 1, borderColor: 'black' }}
        keyboardType='numeric'
      />
      <Text></Text>
      <Button
        onPress={hitung}
        title="Process"
      />
      <Text></Text>
      <Text>Result : {resultAB}</Text>
      <Text></Text>
      <Button title="Tampilkan List" onPress={() => navigate('List')} />
      <StatusBar style="auto" />
    </View>
  );
}


function ListTitle() {

  const [Title, setTitle] = useState([]);

  Axios.get('https://jsonplaceholder.typicode.com/albums').then(function (response) {
    console.log(response.data);
    setTitle(response.data)
  })
    .catch(function (error) {
      console.log(error);
    });

  return (
    <ScrollView>
      {
        Title.map((data, index) => {
          return (
            <Card key={index}><Text>{data.title}</Text></Card>)
        })
      }
    </ScrollView>

  );
}

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false, }} />
        <Stack.Screen name="List" component={ListTitle} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App;