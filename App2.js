import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ToastAndroid } from 'react-native';
import Axios from 'axios';
import axios from 'axios';

export default function App() {

  const [nilaiA, setNilaiA] = useState(0);
  const [nilaiB, setNilaiB] = useState(0);
  const [resultAB, setResultAB] = useState(0);
  const [kategori, setKategori] = useState([]);

  function hitung() {
    if (nilaiA == '' && nilaiB == '') {
      ToastAndroid.show('Kotak A & B Harus di isi.', ToastAndroid.SHORT)
    } else
      if (nilaiA == '') {
        ToastAndroid.show('Kotak A Harus di isi.', ToastAndroid.SHORT)
      } else if (nilaiB == '') {
        ToastAndroid.show('Kotak B Harus di isi.', ToastAndroid.SHORT)
      }
      else {
        setResultAB(parseInt(nilaiA) + parseInt(nilaiB))
        Axios.get('https://developers.zomato.com/api/v2.1/categories', { headers: { 'user-key': 'd7bda05f720629fa168dffbe9926c78e' } }).then(function (response) {
          console.log(response.data.categories);
          setKategori(response.data.categories)
        })
          .catch(function (error) {
            console.log(error);
          });
      }
  }

  return (
    <View style={styles.container}>
      <Text>Tugas Pertama : Penjumlahan</Text>
      <Text></Text>
      <TextInput
        onChangeText={Text => setNilaiA(Text)}
        style={{ textAlign: 'center', height: 40, width: 50, borderWidth: 1, borderColor: 'black' }}
        keyboardType='numeric'
      />
      <Text>+</Text>
      <TextInput
        onChangeText={Text => setNilaiB(Text)}
        style={{ textAlign: 'center', height: 40, width: 50, borderWidth: 1, borderColor: 'black' }}
        keyboardType='numeric'
      />
      <Text></Text>
      <Button
        onPress={hitung}
        title="Process"
      />
      <Text></Text>
      <Text>Result : {resultAB}</Text>
      <Text></Text>
      {
            kategori.map((data, index) => {
              return (<Text key={index}>{data.categories.name}</Text>)
            })
      }
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
